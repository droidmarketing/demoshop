import React, { Fragment } from 'react';
import MailingList from './MailingList';

const Footer = () => (
  <Fragment>
    <MailingList />

    <footer className="push">
      <div className="content">
        <div className="footer-content">
          <div className="footer-about">
            <div className="footer-header">
              <span>Droid Demo</span>
            </div>
            <p>
              This is a Demo Shop made by DroidMarketing
            </p>
          </div>
        </div>
      </div>
    </footer>
  </Fragment>
);

export default Footer;
