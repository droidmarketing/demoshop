import React from 'react';

const MailingList = () => (
  <section className="mailing-list">
    <div className="content">
      <div className="mailing-list-content">
        <h2>
          Do you <span className="love">love</span> Droid?
        </h2>
        <p>
          Sign up to recieve product news, promotions and updates.
        </p>
        <form className="newsletter-form" method="post" noValidate>
          <select>
            <option hidden >Anrede</option>
            <option value="Herr">Herr</option>
            <option value="Frau">Frau</option>
          </select>
          <input
            className="firstname"
            placeholder="Vorname"
            name="firsname"
            type="text"
            aria-label="Firstname"
          />
          <input
            className="lastname"
            placeholder="Nachname"
            name="lastname"
            type="text"
            aria-label="Lastname"
          />
          <input
            className="telephone"
            placeholder="Telefonnummer"
            name="telephone"
            type="tel"
            aria-label="telephone"
          />
          <input
            className="email"
            required="required"
            placeholder="Email address *"
            name="email"
            type="email"
            aria-label="Email"
          />
          <button type="submit" className="submit">
            Sign up
          </button>
        </form>
      </div>
    </div>
  </section>
);

export default MailingList;
