import React from 'react';

const OrderConfirmation = () => (
  <main role="main" id="container" className="main-container push">
    <img src="" alt="banner" id="banner_here" style={{ margin: '0 auto', display: 'none' }} />
    <section className="order-confirmation">
      <div className="content">
        <div className="confirmation">
          <h2>Awesome, your order has been placed</h2>
          {/* <p>Make sure you check your emails for confirmation.</p> */}
          <p>Droid Marketing Demo Shop</p>
          {/* <img src={WeLoveYou} alt="We Love You" /> */}
        </div>
      </div>
    </section>
  </main>
);

export default OrderConfirmation;
