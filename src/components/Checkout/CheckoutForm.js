import React, { Component } from 'react';
import CheckoutSummary from './CheckoutSummary';
import { Field, reduxForm } from 'redux-form';
import * as api from '../../moltin';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';

import { SUBMIT_PAYMENT, PAYMENT_COMPLETE } from '../../ducks/payments';

function mapStateToProps(state) {
  return { push: state.push };
}

var CheckoutTemplate = {
  customer: {
    name: 'Test Customer',
    email: 'customer@droidmarketing.com'
  },
  shipping_address: {
    first_name: 'Test',
    last_name: 'Customer',
    line_1: 'Mariahilfer Straße',
    line_2: '18/1',
    city: 'Vienna',
    postcode: '1070',
    county: 'Vienna',
    country: 'AT'
  },
  billing_address: {
    first_name: 'Test',
    last_name: 'Customer',
    line_1: 'Mariahilfer Straße',
    line_2: '18/1',
    city: 'Vienna',
    postcode: '1070',
    county: 'Vienna',
    country: 'AT'
  }
};
var PaymentTemplate = {
  gateway: 'stripe',
  method: 'purchase',
  first_name: 'Test',
  last_name: 'Customer',
  number: '4242424242424242',
  month: '08',
  year: '2020',
  verification_value: '123'
};

class CheckoutForm extends Component {
  handleKeyDown = function(e) {
    if (e.key === 'Enter' && e.shiftKey === false) {
      e.preventDefault();
    }
  };

  mySubmit = values => {
    let orderData = { customer: {}, billing_address: {}, shipping_address: {}};
    orderData.customer.name = values.name;
    orderData.customer.email = values.email;
    // orderData.customer.id = "c8c1c511-beef-4812-9b7a-9f92c587217c";

    orderData.billing_address.first_name = values.billing_firstname;
    orderData.billing_address.last_name = values.billing_lastname;
    orderData.billing_address.line_1 = values.billing_address_1;
    orderData.billing_address.line_2 = values.billing_address_2;
    orderData.billing_address.city = values.billing_state;
    orderData.billing_address.county = values.billing_state;
    orderData.billing_address.country = values.billing_country;
    orderData.billing_address.company_name = 'Droid Marketing';
    orderData.billing_address.postcode = '1070';

    orderData.shipping_address.first_name = values.shipping_firstname;
    orderData.shipping_address.last_name = values.shipping_lastname;
    orderData.shipping_address.line_1 = values.shipping_address_1;
    orderData.shipping_address.line_2 = values.shipping_address_2;
    orderData.shipping_address.city = values.shipping_state;
    orderData.shipping_address.county = values.shipping_state;
    orderData.shipping_address.country = values.shipping_country;
    orderData.shipping_address.company_name = 'Droid Marketing';
    orderData.shipping_address.postcode = '1070';

    this.props.dispatch(dispatch => {
      dispatch({ type: SUBMIT_PAYMENT });
    });

    api.Checkout(orderData).then(order => {
      api.OrderPay(order.data.id, PaymentTemplate);
      api.DeleteCart();
    }).then(() => {
      this.props.dispatch(dispatch => {
        dispatch({ type: PAYMENT_COMPLETE });
        dispatch(push('/order-confirmation'));
      });
    }).catch(e => {
      console.log(e);
    });
  };

  render() {
    return (
      <main role="main" id="container" className="main-container push">
        <img src="" alt="banner" id="banner_here" style={{ margin: '0 auto', display: 'none' }} />
        <section className="checkout">
          <div className="content">
            <CheckoutSummary />
            <form
              className="checkout-form"
              noValidate
              onSubmit={this.props.handleSubmit(this.mySubmit)}
              onKeyDown={e => {
                this.handleKeyDown(e);
              }}>
              <fieldset className="details completed collapsed">
                <div className="form-header">
                  <h2>Your details</h2>
                </div>
                <div className="form-content">
                  <div className="form-fields">
                    <label className="input-wrap name required">
                      <span className="hide-content">Name</span>
                      <Field
                        component="input"
                        className="name"
                        required="required"
                        placeholder="Name"
                        name="name"
                        type="text"
                        aria-label="Name"
                      />
                    </label>
                    <label className="input-wrap email required">
                      <span className="hide-content">Email address</span>
                      <Field
                        component="input"
                        className="email"
                        required="required"
                        placeholder="Email address"
                        name="email"
                        type="email"
                        aria-label="Email"
                      />
                    </label>
                  </div>
                  <button type="button" className="continue">
                    Continue
                  </button>
                </div>
              </fieldset>
              <fieldset className="billing completed collapsed">
                <div className="form-header inactive">
                  <h2>Billing address</h2>
                </div>
                <div className="form-content">
                  <div className="form-fields">
                    <label className="input-wrap firstname required">
                      <span className="hide-content">First name</span>
                      <Field
                        component="input"
                        required="required"
                        placeholder="First Name"
                        name="billing_firstname"
                        type="text"
                        aria-label="First name"
                      />
                    </label>
                    <label className="input-wrap lastname required">
                      <span className="hide-content">Last name</span>
                      <Field
                        component="input"
                        required="required"
                        placeholder="Last Name"
                        name="billing_lastname"
                        type="text"
                        aria-label="Last name"
                      />
                    </label>
                    <label className="input-wrap company">
                      <span className="hide-content">Company</span>
                      <Field
                        component="input"
                        placeholder="Company"
                        name="billing-company"
                        type="text"
                        aria-label="Company"
                      />
                    </label>
                    <label className="input-wrap address-1 required">
                      <span className="hide-content">Address line 1</span>
                      <Field
                        component="input"
                        required="required"
                        placeholder="Address Line 1"
                        name="billing_address_1"
                        type="text"
                        aria-label="Address line 1"
                      />
                    </label>
                    <label className="input-wrap address-2">
                      <span className="hide-content">Address line 2</span>
                      <Field
                        component="input"
                        placeholder="Address Line 2"
                        name="billing_address_2"
                        type="text"
                        aria-label="Address line 2"
                      />
                    </label>
                    <label className="input-wrap state required">
                      <span className="hide-content">State or county</span>
                      <Field
                        component="input"
                        required="required"
                        placeholder="State / County"
                        name="billing_state"
                        type="text"
                        aria-label="State / County"
                      />
                    </label>
                    <label className="input-wrap postcode required">
                      <span className="hide-content">Postcode</span>
                      <Field
                        component="input"
                        required="required"
                        placeholder="Postcode"
                        name="billing_postcode"
                        type="text"
                        aria-label="Postcode"
                      />
                    </label>
                    <div className="input-wrap country">
                      <label className="required select-fallback">
                        <span className="hide-content">Country</span>
                        <Field
                          component="select"
                          id="billing_country"
                          required="required"
                          name="billing_country">
                          <option value="">Country</option>
                          <option value="AL">Albania</option>
                          <option value="AD">Andorra</option>
                          <option value="AQ">Antarctica</option>
                          <option value="AR">Argentina</option>
                          <option value="AM">Armenia</option>
                          <option value="AT">Austria</option>
                          <option value="AZ">Azerbaijan</option>
                          <option value="BS">Bahamas</option>
                          <option value="BB">Barbados</option>
                          <option value="BY">Belarus</option>
                          <option value="BE">Belgium</option>
                          <option value="BZ">Belize</option>
                          <option value="BJ">Benin</option>
                          <option value="BM">Bermuda</option>
                          <option value="BT">Bhutan</option>
                          <option value="BA">Bosnia and Herzegovina</option>
                          <option value="BR">Brazil</option>
                          <option value="BG">Bulgaria</option>
                          <option value="KH">Cambodia</option>
                          <option value="CA">Canada</option>
                          <option value="KY">Cayman Islands</option>
                          <option value="CL">Chile</option>
                          <option value="CN">China</option>
                          <option value="CX">Christmas Island</option>
                          <option value="CO">Colombia</option>
                          <option value="CR">Costa Rica</option>
                          <option value="CI">Côte d'Ivoire</option>
                          <option value="HR">Croatia</option>
                          <option value="CU">Cuba</option>
                          <option value="CY">Cyprus</option>
                          <option value="CZ">Czech Republic</option>
                          <option value="DK">Denmark</option>
                          <option value="DJ">Djibouti</option>
                          <option value="DO">Dominican Republic</option>
                          <option value="EC">Ecuador</option>
                          <option value="EG">Egypt</option>
                          <option value="EE">Estonia</option>
                          <option value="FK">
                            Falkland Islands (Malvinas)
                          </option>
                          <option value="FO">Faroe Islands</option>
                          <option value="FJ">Fiji</option>
                          <option value="FI">Finland</option>
                          <option value="FR">France</option>
                          <option value="DE">Germany</option>
                          <option value="GI">Gibraltar</option>
                          <option value="GR">Greece</option>
                          <option value="GL">Greenland</option>
                          <option value="GD">Grenada</option>
                          <option value="GU">Guam</option>
                          <option value="GT">Guatemala</option>
                          <option value="HT">Haiti</option>
                          <option value="HM">
                            Heard Island and McDonald Islands
                          </option>
                          <option value="VA">
                            Holy See (Vatican City State)
                          </option>
                          <option value="HU">Hungary</option>
                          <option value="IS">Iceland</option>
                          <option value="IN">India</option>
                          <option value="IR">Iran, Islamic Republic of</option>
                          <option value="IQ">Iraq</option>
                          <option value="IE">Ireland</option>
                          <option value="IM">Isle of Man</option>
                          <option value="IL">Israel</option>
                          <option value="IT">Italy</option>
                          <option value="JM">Jamaica</option>
                          <option value="JP">Japan</option>
                          <option value="KP">
                            Korea, Democratic People's Republic of
                          </option>
                          <option value="KG">Kyrgyzstan</option>
                          <option value="LV">Latvia</option>
                          <option value="LI">Liechtenstein</option>
                          <option value="LT">Lithuania</option>
                          <option value="LU">Luxembourg</option>
                          <option value="MK">Northmacedonia</option>
                          <option value="MV">Maldives</option>
                          <option value="MT">Malta</option>
                          <option value="MH">Marshall Islands</option>
                          <option value="MX">Mexico</option>
                          <option value="MD">Moldova, Republic of</option>
                          <option value="MC">Monaco</option>
                          <option value="NL">Netherlands</option>
                          <option value="NO">Norway</option>
                          <option value="PL">Poland</option>
                          <option value="PT">Portugal</option>
                          <option value="RO">Romania</option>
                          <option value="RU">Russian Federation</option>
                          <option value="SM">San Marino</option>
                          <option value="RS">Serbia</option>
                          <option value="SK">Slovakia</option>
                          <option value="SI">Slovenia</option>
                          <option value="SO">Somalia</option>
                          <option value="ES">Spain</option>
                          <option value="SZ">Swaziland</option>
                          <option value="SE">Sweden</option>
                          <option value="CH">Switzerland</option>
                          <option value="TW">Taiwan</option>
                          <option value="TJ">Tajikistan</option>
                          <option value="TG">Togo</option>
                          <option value="TO">Tonga</option>
                          <option value="TR">Turkey</option>
                          <option value="TM">Turkmenistan</option>
                          <option value="TV">Tuvalu</option>
                          <option value="UG">Uganda</option>
                          <option value="UA">Ukraine</option>
                          <option value="GB">United Kingdom</option>
                          <option value="US">United States</option>
                          <option value="UY">Uruguay</option>
                          <option value="UZ">Uzbekistan</option>
                          <option value="VU">Vanuatu</option>
                        </Field>
                      </label>
                    </div>
                  </div>
                  <button type="button" className="continue">
                    Continue
                  </button>
                </div>
              </fieldset>
              <fieldset className="shipping completed collapsed">
                <div className="form-header inactive">
                  <h2>Shipping address</h2>
                </div>
                <div className="form-content">
                  <div className="form-fields">
                    <label className="input-wrap firstname required">
                      <span className="hide-content">First name</span>
                      <Field
                        component="input"
                        required="required"
                        placeholder="First Name"
                        name="shipping_firstname"
                        type="text"
                        aria-label="First name"
                      />
                    </label>
                    <label className="input-wrap lastname required">
                      <span className="hide-content">Last name</span>
                      <Field
                        component="input"
                        required="required"
                        placeholder="Last Name"
                        name="shipping_lastname"
                        type="text"
                        aria-label="Last name"
                      />
                    </label>
                    <label className="input-wrap company">
                      <span className="hide-content">Company</span>
                      <Field
                        component="input"
                        placeholder="Company"
                        name="shipping_company"
                        type="text"
                        aria-label="Company"
                      />
                    </label>
                    <label className="input-wrap address-1 required">
                      <span className="hide-content">Address line 1</span>
                      <Field
                        component="input"
                        required="required"
                        placeholder="Address Line 1"
                        name="shipping_address_1"
                        type="text"
                        aria-label="Address line 1"
                      />
                    </label>
                    <label className="input-wrap address-2">
                      <span className="hide-content">Address line 2</span>
                      <Field
                        component="input"
                        placeholder="Address Line 2"
                        name="shipping_address_2"
                        type="text"
                        aria-label="Address line 2"
                      />
                    </label>
                    <label className="input-wrap state required">
                      <span className="hide-content">State or county</span>
                      <Field
                        component="input"
                        required="required"
                        placeholder="State / County"
                        name="shipping_state"
                        type="text"
                        aria-label="State / County"
                      />
                    </label>
                    <label className="input-wrap postcode required">
                      <span className="hide-content">Postcode</span>
                      <Field
                        component="input"
                        required="required"
                        placeholder="Postcode"
                        name="shipping_postcode"
                        type="text"
                        aria-label="Postcode"
                      />
                    </label>
                    <div className="input-wrap country">
                      <label className="select-fallback required">
                        <span className="hide-content">Country</span>
                        <Field
                          component="select"
                          id="shipping_country"
                          required="required"
                          name="shipping_country">
                          <option value="">Country</option>
                          <option value="AL">Albania</option>
                          <option value="AD">Andorra</option>
                          <option value="AQ">Antarctica</option>
                          <option value="AR">Argentina</option>
                          <option value="AM">Armenia</option>
                          <option value="AT">Austria</option>
                          <option value="AZ">Azerbaijan</option>
                          <option value="BS">Bahamas</option>
                          <option value="BB">Barbados</option>
                          <option value="BY">Belarus</option>
                          <option value="BE">Belgium</option>
                          <option value="BZ">Belize</option>
                          <option value="BJ">Benin</option>
                          <option value="BM">Bermuda</option>
                          <option value="BT">Bhutan</option>
                          <option value="BA">Bosnia and Herzegovina</option>
                          <option value="BR">Brazil</option>
                          <option value="BG">Bulgaria</option>
                          <option value="KH">Cambodia</option>
                          <option value="CA">Canada</option>
                          <option value="KY">Cayman Islands</option>
                          <option value="CL">Chile</option>
                          <option value="CN">China</option>
                          <option value="CX">Christmas Island</option>
                          <option value="CO">Colombia</option>
                          <option value="CR">Costa Rica</option>
                          <option value="CI">Côte d'Ivoire</option>
                          <option value="HR">Croatia</option>
                          <option value="CU">Cuba</option>
                          <option value="CY">Cyprus</option>
                          <option value="CZ">Czech Republic</option>
                          <option value="DK">Denmark</option>
                          <option value="DJ">Djibouti</option>
                          <option value="DO">Dominican Republic</option>
                          <option value="EC">Ecuador</option>
                          <option value="EG">Egypt</option>
                          <option value="EE">Estonia</option>
                          <option value="FK">
                            Falkland Islands (Malvinas)
                          </option>
                          <option value="FO">Faroe Islands</option>
                          <option value="FJ">Fiji</option>
                          <option value="FI">Finland</option>
                          <option value="FR">France</option>
                          <option value="DE">Germany</option>
                          <option value="GI">Gibraltar</option>
                          <option value="GR">Greece</option>
                          <option value="GL">Greenland</option>
                          <option value="GD">Grenada</option>
                          <option value="GU">Guam</option>
                          <option value="GT">Guatemala</option>
                          <option value="HT">Haiti</option>
                          <option value="HM">
                            Heard Island and McDonald Islands
                          </option>
                          <option value="VA">
                            Holy See (Vatican City State)
                          </option>
                          <option value="HU">Hungary</option>
                          <option value="IS">Iceland</option>
                          <option value="IN">India</option>
                          <option value="IR">Iran, Islamic Republic of</option>
                          <option value="IQ">Iraq</option>
                          <option value="IE">Ireland</option>
                          <option value="IM">Isle of Man</option>
                          <option value="IL">Israel</option>
                          <option value="IT">Italy</option>
                          <option value="JM">Jamaica</option>
                          <option value="JP">Japan</option>
                          <option value="KP">
                            Korea, Democratic People's Republic of
                          </option>
                          <option value="KG">Kyrgyzstan</option>
                          <option value="LV">Latvia</option>
                          <option value="LI">Liechtenstein</option>
                          <option value="LT">Lithuania</option>
                          <option value="LU">Luxembourg</option>
                          <option value="MK">Northmacedonia</option>
                          <option value="MV">Maldives</option>
                          <option value="MT">Malta</option>
                          <option value="MH">Marshall Islands</option>
                          <option value="MX">Mexico</option>
                          <option value="MD">Moldova, Republic of</option>
                          <option value="MC">Monaco</option>
                          <option value="NL">Netherlands</option>
                          <option value="NO">Norway</option>
                          <option value="PL">Poland</option>
                          <option value="PT">Portugal</option>
                          <option value="RO">Romania</option>
                          <option value="RU">Russian Federation</option>
                          <option value="SM">San Marino</option>
                          <option value="RS">Serbia</option>
                          <option value="SK">Slovakia</option>
                          <option value="SI">Slovenia</option>
                          <option value="SO">Somalia</option>
                          <option value="ES">Spain</option>
                          <option value="SZ">Swaziland</option>
                          <option value="SE">Sweden</option>
                          <option value="CH">Switzerland</option>
                          <option value="TW">Taiwan</option>
                          <option value="TJ">Tajikistan</option>
                          <option value="TG">Togo</option>
                          <option value="TO">Tonga</option>
                          <option value="TR">Turkey</option>
                          <option value="TM">Turkmenistan</option>
                          <option value="TV">Tuvalu</option>
                          <option value="UG">Uganda</option>
                          <option value="UA">Ukraine</option>
                          <option value="GB">United Kingdom</option>
                          <option value="US">United States</option>
                          <option value="UY">Uruguay</option>
                          <option value="UZ">Uzbekistan</option>
                          <option value="VU">Vanuatu</option>
                        </Field>
                      </label>
                    </div>
                  </div>
                  <button type="button" className="continue">
                    Continue
                  </button>
                </div>
              </fieldset>
              <fieldset className="payment completed">
                <div className="form-header inactive">
                  <h2>Payment details</h2>
                </div>
                <div className="form-content">
                  <div className="form-fields">
                    <label className="input-wrap name">
                      <span className="hide-content">Name on card</span>
                      <Field
                        component="input"
                        required="required"
                        placeholder="Name on card"
                        name="card_name"
                        type="text"
                        aria-label="Name on card"
                      />
                    </label>
                    <label className="input-wrap card required">
                      <span className="hide-content">Card number</span>
                      <Field
                        component="input"
                        required="required"
                        placeholder="Card number"
                        name="card-number"
                        maxLength="23"
                        type="tel"
                        aria-label="Card number"
                      />
                    </label>
                    <div className="input-wrap expiry-month">
                      <label className="select-fallback required">
                        <span className="hide-content">Card expiry month</span>
                        <select
                          id="expiry-month"
                          required="required"
                          name="expiry-month">
                          <option value="01">January</option>
                          <option value="02">February</option>
                          <option value="03">March</option>
                          <option value="04">April</option>
                          <option value="05">May</option>
                          <option value="06">June</option>
                          <option value="07">July</option>
                          <option value="08">August</option>
                          <option value="09">September</option>
                          <option value="10">October</option>
                          <option value="11">November</option>
                          <option value="12">December</option>
                        </select>
                      </label>
                    </div>
                    <div className="input-wrap expiry-year">
                      <label className="select-fallback required">
                        <span className="hide-content">Card expiry year</span>
                        <select
                          id="expiry-year"
                          required="required"
                          name="expiry-year">
                          <option value="2017">2017</option>
                          <option value="2018">2018</option>
                          <option value="2019">2019</option>
                          <option value="2020">2020</option>
                          <option value="2021">2021</option>
                          <option value="2022">2022</option>
                          <option value="2023">2023</option>
                          <option value="2024">2024</option>
                          <option value="2025">2025</option>
                          <option value="2026">2026</option>
                          <option value="2027">2027</option>
                        </select>
                      </label>
                    </div>
                    <label className="input-wrap cvc required">
                      <span className="hide-content">CVC code</span>
                      <Field
                        component="input"
                        required="required"
                        placeholder="CVC"
                        maxLength="4"
                        name="card_cvc"
                        type="tel"
                        aria-label="CVC"
                      />
                    </label>
                  </div>
                  <button type="submit" className="pay" aria-live="polite">
                    <div className="loading-icon">
                      <span className="hide-content">Processing</span>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 52.7 46.9"
                        aria-hidden="true">
                        <path
                          fill="currentColor"
                          d="M47.8,15.9c0,2.8-1,5.6-3.2,7.6L26.4,41.7L8.1,23.5c-4.3-4.3-4.3-11.1,0-15.4c2.1-2.1,4.9-3.2,7.7-3.2c2.8,0,5.6,1,7.6,3.2
                            l2.9,2.9l2.9-2.9c4.3-4.3,11.1-4.3,15.4,0C46.7,10.3,47.8,13.1,47.8,15.9z"
                        />
                      </svg>
                    </div>
                    <span className="copy">Pay</span>
                  </button>
                </div>
              </fieldset>
            </form>
          </div>
        </section>
      </main>
    );
  }
}

CheckoutForm = reduxForm({
  form: 'CheckoutForm',
  initialValues: {
    'name': CheckoutTemplate.customer.name,
    'email': CheckoutTemplate.customer.email,
    'billing_firstname': CheckoutTemplate.billing_address.first_name,
    'billing_lastname': CheckoutTemplate.billing_address.last_name,
    'billing-company': 'Droid Marketing',
    'billing_address_1': CheckoutTemplate.billing_address.line_1,
    'billing_address_2': CheckoutTemplate.billing_address.line_2,
    'billing_state': CheckoutTemplate.billing_address.county,
    'billing_postcode': CheckoutTemplate.billing_address.postcode,
    'billing_country': CheckoutTemplate.billing_address.country,
    'shipping_firstname': CheckoutTemplate.shipping_address.first_name,
    'shipping_lastname': CheckoutTemplate.shipping_address.last_name,
    'shipping_company': 'Droid Marketing',
    'shipping_address_1': CheckoutTemplate.shipping_address.line_1,
    'shipping_address_2': CheckoutTemplate.shipping_address.line_2,
    'shipping_state': CheckoutTemplate.shipping_address.county,
    'shipping_postcode': CheckoutTemplate.shipping_address.postcode,
    'shipping_country': CheckoutTemplate.shipping_address.country,
    'card_name': PaymentTemplate.first_name + ' ' + PaymentTemplate.last_name,
    'card-number': PaymentTemplate.number,
    'expiry-month': PaymentTemplate.month,
    'expiry-year': PaymentTemplate.year,
    'card_cvc': PaymentTemplate.verification_value
  }
})(CheckoutForm);

export default connect(mapStateToProps)(CheckoutForm);