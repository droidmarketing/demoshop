import React from 'react';
import TopPicksContainer from './TopPicksContainer';
import CategoriesContainer from '../Categories/CategoriesContainer';

const HomeMainSection = () => (
  <main role="main" id="container" className="main-container push">
    <img src="" alt="banner" id="banner_here" style={{ margin: '0 auto', display: 'none' }} />
    <CategoriesContainer />
    <TopPicksContainer />
  </main>
);

export default HomeMainSection;
